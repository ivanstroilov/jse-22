package ru.t1.stroilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    public final static String DESCRIPTION = "Unlock User.";

    @NotNull
    public final static String NAME = "user-unlock";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
