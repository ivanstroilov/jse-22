package ru.t1.stroilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.ICommandRepository;
import ru.t1.stroilov.tm.command.AbstractCommand;
import ru.t1.stroilov.tm.model.Command;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    private static final Command[] COMMANDS_ARRAY = new Command[]{

    };

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        final String name = command.getName();
        if (!name.isEmpty()) mapByName.put(name, command);
        final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @NotNull
    public Command[] getCommandsArray() {
        return COMMANDS_ARRAY;
    }

}
