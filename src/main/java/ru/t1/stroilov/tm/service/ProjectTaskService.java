package ru.t1.stroilov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.api.service.IProjectTaskService;
import ru.t1.stroilov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stroilov.tm.exception.entity.TaskNotFoundException;
import ru.t1.stroilov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.stroilov.tm.exception.field.TaskIdEmptyException;
import ru.t1.stroilov.tm.exception.field.UserIdEmptyException;
import ru.t1.stroilov.tm.model.Project;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public Task bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectID(userId, projectId);
        for (final Task task : tasks) taskRepository.deleteById(task.getId());
        projectRepository.deleteById(userId, projectId);
    }

    @Override
    public void removeProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<Project> projects = projectRepository.findAll(userId);
        for (final Project project : projects) {
            if (project == null) continue;
            removeProjectById(userId, project.getId());
        }
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }
}
