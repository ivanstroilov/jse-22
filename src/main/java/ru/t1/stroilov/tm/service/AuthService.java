package ru.t1.stroilov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.service.IAuthService;
import ru.t1.stroilov.tm.api.service.IPropertyService;
import ru.t1.stroilov.tm.api.service.IUserService;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.exception.field.LoginEmptyException;
import ru.t1.stroilov.tm.exception.field.PasswordEmptyException;
import ru.t1.stroilov.tm.exception.user.AccessDeniedException;
import ru.t1.stroilov.tm.exception.user.PermissionException;
import ru.t1.stroilov.tm.exception.user.UserNotLoggedInException;
import ru.t1.stroilov.tm.model.User;
import ru.t1.stroilov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        @Nullable final boolean locked = user.isLocked();
        if (locked) throw new PermissionException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || !hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Nullable
    @NotNull
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new UserNotLoggedInException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new UserNotLoggedInException();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @Nullable final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }
}
