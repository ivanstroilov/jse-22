package ru.t1.stroilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.enumerated.Sort;
import ru.t1.stroilov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void deleteAll(@NotNull String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    int getSize(@NotNull String userId);

    @Nullable
    M deleteById(@Nullable String userId, @Nullable String id);

    @Nullable
    M deleteByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

    @Nullable
    M delete(@Nullable String userId, @Nullable M model);

}
