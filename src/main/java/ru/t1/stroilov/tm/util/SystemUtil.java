package ru.t1.stroilov.tm.util;

import org.jetbrains.annotations.Nullable;

import java.lang.management.ManagementFactory;

public interface SystemUtil {

    static long getPID() {
        @Nullable final String processName = ManagementFactory.getRuntimeMXBean().getName();
        if (processName == null || processName.isEmpty()) {
            return 0;
        }
        try {
            return Long.parseLong(processName.split("@")[0]);
        } catch (Exception e) {
            return 0;
        }
    }

}
