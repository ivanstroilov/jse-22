package ru.t1.stroilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.IUserOwnedRepository;
import ru.t1.stroilov.tm.enumerated.Sort;
import ru.t1.stroilov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    protected Predicate<M> filterByUserId(@NotNull final String userId) {
        return m -> userId.equals(m.getUserId());
    }


    @Override
    public void deleteAll(@NotNull final String userId) {
        final List<M> models = findAll(userId);
        deleteAll(models);
    }

    @Nullable
    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return findAll().stream().filter(filterByUserId(userId)).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<M> comparator = (Comparator<M>) sort.getComparator();
        if (comparator == null) return findAll(userId);
        return findAll(comparator);
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return findAll().stream().filter(filterByUserId(userId)).filter(filterById(id)).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        return findAll().stream().filter(filterByUserId(userId)).skip(index).findFirst().orElse(null);
    }

    @NotNull
    @Override
    public int getSize(@Nullable final String userId) {
        return (int) findAll().stream().filter(filterByUserId(userId)).count();
    }

    @Nullable
    @Override
    public M deleteById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return delete(model);
    }

    @Nullable
    @Override
    public M deleteByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) return null;
        final M model = findByIndex(userId, index);
        if (model == null) return null;
        return delete(model);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null) return null;
        if (model == null) return null;
        model.setUserId(userId);
        models.put(model.getId(), model);
        return model;
    }

    @Nullable
    @Override
    public M delete(@Nullable final String userId, @Nullable final M model) {
        if (userId == null) return null;
        if (model == null) return null;
        return deleteById(userId, model.getId());
    }
}
