package ru.t1.stroilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.IRepository;
import ru.t1.stroilov.tm.model.AbstractModel;

import java.util.*;
import java.util.function.Predicate;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Map<String, M> models = new LinkedHashMap<>();

    @NotNull
    protected Predicate<M> filterById(@NotNull final String id) {
        return m -> id.equals(m.getId());
    }

    @Override
    public void deleteAll() {
        models.clear();
    }

    @Override
    public void deleteAll(@NotNull final Collection<M> collection) {
        collection.stream().map(AbstractModel::getId).forEach(models::remove);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return new ArrayList<>(models.values());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(models.values());
        result.sort(comparator);
        return result;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.put(model.getId(), model);
        return model;
    }

    @NotNull
    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @Nullable
    @Override
    public M findById(@NotNull final String id) {
        return models.get(id);
    }

    @Nullable
    @Override
    public M findByIndex(@NotNull final Integer index) {
        return models.values().stream().skip(index).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M delete(@Nullable final M model) {
        if (model == null) return null;
        models.remove(model.getId());
        return model;
    }

    @Nullable
    @Override
    public M deleteById(@NotNull final String id) {
        final M model = findById(id);
        if (model == null) return null;
        return delete(model);
    }

    @Nullable
    @Override
    public M deleteByIndex(@NotNull final Integer index) {
        final M model = findByIndex(index);
        if (model == null) return null;
        return delete(model);
    }

    @NotNull
    @Override
    public int getSize() {
        return models.size();
    }
}
