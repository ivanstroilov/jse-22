package ru.t1.stroilov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.stroilov.tm.api.repository.ICommandRepository;
import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.api.repository.IUserRepository;
import ru.t1.stroilov.tm.api.service.*;
import ru.t1.stroilov.tm.command.AbstractCommand;
import ru.t1.stroilov.tm.command.project.*;
import ru.t1.stroilov.tm.command.system.*;
import ru.t1.stroilov.tm.command.task.*;
import ru.t1.stroilov.tm.command.user.*;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.exception.system.CommandNotSupportedException;
import ru.t1.stroilov.tm.model.Project;
import ru.t1.stroilov.tm.model.Task;
import ru.t1.stroilov.tm.repository.CommandRepository;
import ru.t1.stroilov.tm.repository.ProjectRepository;
import ru.t1.stroilov.tm.repository.TaskRepository;
import ru.t1.stroilov.tm.repository.UserRepository;
import ru.t1.stroilov.tm.service.*;
import ru.t1.stroilov.tm.util.SystemUtil;
import ru.t1.stroilov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @NotNull
    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new ApplicationDeveloperInfoCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserDeleteCommand());
        registry(new UserEditCommand());
        registry(new UserLockCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUnlockCommand());
        registry(new UserViewCommand());
    }

    public void run(@Nullable String... args) {
        if (processArgument(args)) processCommand("exit");
        initPID();
        initDemoData();
        initLogger();

        System.out.println("Please enter command: ");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                @Nullable final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                processCommand(command);
                System.out.println();
            } catch (final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> LOGGER_LIFECYCLE.info("*** TASK MANAGER IS SHUTTING DOWN ***")));
    }

    private void initDemoData() {
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("test", "test", "test@test.test");
        userService.create("user", "user", "user@user.user");
        projectService.add(new Project("Project1", Status.COMPLETED));
        projectService.add(new Project("Project2", Status.IN_PROGRESS));
        projectService.add(new Project("Project3", Status.IN_PROGRESS));
        projectService.add(new Project("Project4", Status.NOT_STARTED));
        taskService.add(new Task("Task1", "Task1"));
        taskService.add(new Task("Task2", "Task2"));
        taskService.add(new Task("Task3", "Task3"));
        taskService.add(new Task("Task4", "Task4"));
    }

    public void processArgument(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    @NotNull
    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}