package ru.t1.stroilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.service.IProjectService;
import ru.t1.stroilov.tm.api.service.IProjectTaskService;
import ru.t1.stroilov.tm.command.AbstractCommand;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.model.Project;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    protected void showProjectList(@NotNull final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            System.out.printf("%s[%s]. %s \n", index, project.getId(), project);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
