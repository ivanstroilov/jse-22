package ru.t1.stroilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String DESCRIPTION = "Update Project by index.";

    @NotNull
    public final static String NAME = "project-update-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(getUserId(), index, name, description);
    }
}
