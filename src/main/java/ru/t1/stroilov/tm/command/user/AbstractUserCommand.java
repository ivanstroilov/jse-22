package ru.t1.stroilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.service.IAuthService;
import ru.t1.stroilov.tm.api.service.IUserService;
import ru.t1.stroilov.tm.command.AbstractCommand;
import ru.t1.stroilov.tm.exception.entity.UserNotFoundException;
import ru.t1.stroilov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

}
