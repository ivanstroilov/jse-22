package ru.t1.stroilov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.stroilov.tm.api.service.ISaltProvider;

import java.security.MessageDigest;

public interface HashUtil {

    @NotNull
    Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @Nullable
    static String salt(@Nullable final String value, @Nullable final String secret, @Nullable final Integer iteration) {
        if (value == null || secret == null || iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String salt(@Nullable final ISaltProvider saltProvider, @Nullable final String value) {
        if (saltProvider == null) return null;
        @NotNull final String secret = saltProvider.getPasswordSecret();
        @NotNull final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (final Exception e) {
            LOGGER_LIFECYCLE.error(e.getMessage());
            System.out.println("[FAIL]");
        }
        return null;
    }

}
