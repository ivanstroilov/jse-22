package ru.t1.stroilov.tm.api.component;

import org.jetbrains.annotations.Nullable;

public interface IBootstrap {

    void run(@Nullable String... args);

}