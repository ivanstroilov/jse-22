package ru.t1.stroilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    @Nullable
    Project add(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project add(@Nullable String userId, @Nullable String name);

}
