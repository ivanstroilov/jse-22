package ru.t1.stroilov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.api.repository.IUserRepository;
import ru.t1.stroilov.tm.api.service.IPropertyService;
import ru.t1.stroilov.tm.api.service.IUserService;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.exception.entity.UserNotFoundException;
import ru.t1.stroilov.tm.exception.field.*;
import ru.t1.stroilov.tm.exception.user.EmailExistsException;
import ru.t1.stroilov.tm.exception.user.LoginExistsException;
import ru.t1.stroilov.tm.model.User;
import ru.t1.stroilov.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository repository, @NotNull final ITaskRepository taskRepository, @NotNull final IProjectRepository projectRepository, @NotNull final IPropertyService propertyService) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (loginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (loginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (emailExists(email)) throw new EmailExistsException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (loginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        final User user = repository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        final User user = repository.findByEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User deleteByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return delete(user);
    }

    @NotNull
    @Override
    public User deleteByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return delete(user);
    }

    @NotNull
    @Override
    public User delete(@Nullable final User model) {
        if (model == null) throw new UserNotFoundException();
        final User user = super.delete(model);
        if (user == null) throw new UserNotFoundException();
        @Nullable final String userId = user.getId();
        taskRepository.deleteAll(userId);
        projectRepository.deleteAll(userId);
        return user;
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull
    @Override
    public User updateUser(@Nullable final String id, @NotNull final String firstName, @NotNull final String lastName, @NotNull final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    @Override
    public Boolean loginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.loginExists(login);
    }

    @NotNull
    @Override
    public Boolean emailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.emailExists(email);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
    }
}
