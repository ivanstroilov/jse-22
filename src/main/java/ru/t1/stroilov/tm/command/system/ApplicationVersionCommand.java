package ru.t1.stroilov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public final static String DESCRIPTION = "Display program version.";

    @NotNull
    public final static String NAME = "version";

    @NotNull
    public final static String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }
}
