package ru.t1.stroilov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.api.model.IWBS;
import ru.t1.stroilov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public Project(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (description != null && !description.isEmpty()) result += " : " + description;
        if (status != null) result += " : " + Status.toName(status);
        return result;
    }
}
