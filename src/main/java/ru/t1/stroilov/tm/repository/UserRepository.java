package ru.t1.stroilov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.IUserRepository;
import ru.t1.stroilov.tm.model.User;

@NoArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        return findAll().stream().filter(m -> login.equals(m.getLogin())).findFirst().orElse(null);
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull final String email) {
        return findAll().stream().filter(m -> email.equals(m.getEmail())).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User delete(@Nullable final User user) {
        if (user == null) return null;
        models.remove(user);
        return user;
    }

    @NotNull
    @Override
    public Boolean loginExists(@NotNull final String login) {
        return findAll().stream().anyMatch(m -> login.equals(m.getLogin()));
    }

    @NotNull
    @Override
    public Boolean emailExists(@NotNull final String email) {
        return findAll().stream().anyMatch(m -> email.equals(m.getEmail()));
    }

    @NotNull
    @Override
    public User findById(@NotNull final String id) {
        return findAll().stream().filter(filterById(id)).findFirst().orElse(null);
    }
}
