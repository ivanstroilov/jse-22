# TASK MANAGER

## DEVELOPER INFO

**NAME:** Ivan Stroilov

**E-MAIL:** ivanstroilov@gmail.com

## SYSTEM INFO

**OS**: Ubuntu 22.04

**JDK**: Java 1.8

**RAM**: 16GB

**CPU**: intel G4600

## BUILD PROJECT

```
mvn clean install
```

## RUN PROJECT

```
cd ./target/task-manager/bin
./start.sh
```
