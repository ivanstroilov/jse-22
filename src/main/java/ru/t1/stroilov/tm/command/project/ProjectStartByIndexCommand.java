package ru.t1.stroilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String DESCRIPTION = "Start Project by index.";

    @NotNull
    public final static String NAME = "project-start-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(getUserId(), index, Status.IN_PROGRESS);
    }
}
