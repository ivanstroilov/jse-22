package ru.t1.stroilov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.model.Project;

@NoArgsConstructor
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public Project add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        final Project project = new Project(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @Nullable
    @Override
    public Project add(@Nullable final String userId, @Nullable final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

}
